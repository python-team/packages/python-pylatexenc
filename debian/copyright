Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pylatexenc
Upstream-Contact: Philippe Faist <philippe.faist@bluewin.ch>
Source: https://github.com/phfaist/pylatexenc

Files: *
Copyright: 2015-2019 Philippe Faist
License: Expat

Files: pylatexenc/latexencode/_uni2latexmap.py
Copyright: 2015 Philippe Faist
           2011-2014 Matthias C. M. Troffaes
License: Expat

Files: pylatexenc/latexencode/_uni2latexmap_xml.py
Copyright: 2015-2019 Philippe Faist
           2015 World Wide Web Consortium
           1999-2015 David Carlise
License: Expat and W3C
Comment: The file 'tools/unicode.xml' is used by upstream to generate this
         file (using the 'tools/gen_xml_dic.py' script).

Files: tools/unicode.xml
Copyright: 2015 World Wide Web Consortium
         1999-2015 David Carlise
License: W3C

Files: pylatexenc/__init__.py
Copyright: 2015 Philippe Faist
License: Expat

Files:
 pylatexenc/latex2text/__init__.py
 pylatexenc/latex2text/__main__.py
 pylatexenc/latexencode/__init__.py
 pylatexenc/latexwalker/__init__.py
 pylatexenc/latexwalker/__main__.py
Copyright: 2018 Philippe Faist
License: Expat

Files:
 doc/conf.py
 pylatexenc/latex2text/_defaultspecs.py
 pylatexenc/latexencode/__main__.py
 pylatexenc/latexwalker/_defaultspecs.py
 pylatexenc/macrospec/_argparsers.py
 pylatexenc/macrospec/__init__.py
 pylatexenc/_util.py setup.py
Copyright: 2019 Philippe Faist
License: Expat

Files:
 pylatexenc/latexencode/_partial_latex_encoder.py
 pylatexenc/latexencode/_unicode_to_latex_encoder.py
 pylatexenc/version.py
Copyright: 2021 Philippe Faist
License: Expat

Files: debian/*
Copyright: 2021 Diego M. Rodriguez <diego@moreda.io>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: W3C
 [This notice should be placed within redistributed or derivative software code
 when appropriate. This particular formulation became active on December 31 2002,
 superseding the 1998 version.]
 .
 unicode.xml: https://www.w3.org/2003/entities/2007xml/unicode.xml
              https://www.w3.org/TR/xml-entity-names/#source
 .
     Copyright © 2015 World Wide Web Consortium, (Massachusetts Institute of
     Technology, European Research Consortium for Informatics and Mathematics,
     Keio University, Beihang). All Rights Reserved. This work is distributed
     under the W3C® Software License [1] in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
     or FITNESS FOR A PARTICULAR PURPOSE.
 .
     [1] http://www.w3.org/Consortium/Legal/copyright-software
 .
 .
 ----------
 .
 Copyright David Carlisle 1999-2015
 .
 Use and distribution of this code are permitted under the terms of the
 W3C Software Notice and License.
 http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231.html
 .
 ----------
 .
 LICENSE
 .
 By obtaining, using and/or copying this work, you (the licensee) agree that you
 have read, understood, and will comply with the following terms and conditions.
 .
 Permission to copy, modify, and distribute this software and its documentation,
 with or without modification, for any purpose and without fee or royalty is
 hereby granted, provided that you include the following on ALL copies of the
 software and documentation or portions thereof, including modifications:
 .
     - The full text of this NOTICE in a location viewable to users of the
       redistributed or derivative work.
 .
     - Any pre-existing intellectual property disclaimers, notices, or terms and
       conditions. If none exist, the W3C Software Short Notice should be
       included (hypertext is preferred, text is permitted) within the body of
       any redistributed or derivative code.
 .
     - Notice of any changes or modifications to the files, including the date
       changes were made. (We recommend you provide URIs to the location from
       which the code is derived.)
 .
 DISCLAIMERS
 .
 THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS MAKE
 NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 TO, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT
 THE USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY
 PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR
 CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR DOCUMENTATION.
 .
 The name and trademarks of copyright holders may NOT be used in advertising or
 publicity pertaining to the software without specific, written prior
 permission. Title to copyright in this software and any associated documentation
 will at all times remain with copyright holders.
 .
 NOTES
 .
 This version: http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231
 .
 This formulation of W3C's notice and license became active on December 31
 2002. This version removes the copyright ownership notice such that this license
 can be used with materials other than those owned by the W3C, reflects that
 ERCIM is now a host of the W3C, includes references to this specific dated
 version of the license, and removes the ambiguous grant of "use". Otherwise,
 this version is the same as the previous version and is written so as to
 preserve the Free Software Foundation's assessment of GPL compatibility and
 OSI's certification under the Open Source Definition.
